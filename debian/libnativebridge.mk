NAME = libnativebridge

SOURCES = libnativebridge/native_bridge.cc
OBJECTS = $(SOURCES:.cc=.o)

CPPFLAGS += \
  -I/usr/include/android/nativehelper \
  -Ilibnativebridge/include \

LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -ldl \
  -llog \
  -shared

debian/out/$(NAME).so.0: $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)
	cd debian/out && ln -s $(NAME).so.0 $(NAME).so

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
