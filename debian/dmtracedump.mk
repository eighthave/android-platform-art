NAME = dmtracedump

SOURCES = tracedump.cc
SOURCES := $(foreach source, $(SOURCES), tools/dmtracedump/$(source))
OBJECTS = $(SOURCES:.cc=.o)

CPPFLAGS += \
  -Itools/dmtracedump \

debian/out/$(NAME): $(OBJECTS)
	mkdir -p debian/out
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CPPFLAGS) $(CXXFLAGS)
