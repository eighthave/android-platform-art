Source: android-platform-art
Section: devel
Priority: optional
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders: Hans-Christoph Steiner <hans@eds.org>
Build-Depends:
 android-libbacktrace-dev (>= 1:34) [amd64 i386 armhf arm64 riscv64],
 android-libcutils-dev [amd64 i386 armhf arm64 riscv64],
 android-libnativehelper-dev [amd64 i386 armhf arm64 riscv64],
 android-libziparchive-dev [amd64 i386 armhf arm64 riscv64],
 clang [amd64 i386 armel armhf arm64 mipsel mips64el ppc64el s390x ppc64 sparc64 riscv64],
 debhelper-compat (= 12),
 dh-exec,
 help2man,
 libicu-dev [amd64 i386 armhf arm64 riscv64],
 liblz4-dev [amd64 i386 armhf arm64 riscv64],
 libtinyxml2-dev [amd64 i386 armhf arm64 riscv64],
 lld [amd64 i386 armel armhf arm64 mipsel mips64el ppc64el],
 python3 [amd64 i386 armhf arm64 riscv64],
 zlib1g-dev [amd64 i386 armhf arm64 riscv64],
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://android.googlesource.com/platform/art
Vcs-Git: https://salsa.debian.org/android-tools-team/android-platform-art.git
Vcs-Browser: https://salsa.debian.org/android-tools-team/android-platform-art

Package: dexdump
Architecture: amd64 i386 armhf arm64 riscv64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends},
         android-libbacktrace (>= 1:34),
         android-libnativeloader (= 1:${binary:Version}),
         android-libziparchive,
Description: Displays information about Android DEX files
 The `dexdump` tool is intended to mimic `objdump`. When possible, use
 similar command-line arguments.
 .
 This is a re-implementation of the original `dexdump` utility that was
 based on Dalvik functions in `libdex` into a new `dexdump` that is now
 based on ART functions in `libart` instead. The output is very similar
 to the original for correct DEX files. Error messages may differ,
 however. Also, ODEX files are no longer supported.

Package: dmtracedump
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends},
         graphviz,
Description: Generates graphical call-stack diagrams from Android trace logs
 `dmtracedump` generates the call stack data as a tree diagram, where
 each node represents a method call. It shows call flow (from parent
 node to child nodes) using arrows.

Package: android-libart
Architecture: amd64 i386 armhf arm64 riscv64
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
Description: Android Runtime
 Android Runtime (ART) is the managed runtime used by applications and
 some system services on Android. ART and its predecessor Dalvik were
 originally created specifically for the Android project. ART as the
 runtime executes the Dalvik Executable format and DEX bytecode
 specification.
 .
 This package provides `libart` and `libsigchain`.
 .
 This library is only used by Android SDK and uses a customized RPATH.

Package: android-libnativebridge
Section: libs
Architecture: amd64 i386 armhf arm64 riscv64
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
         android-liblog (>= 1:34),
Description: Android native bridge library
 This library is only used by Android SDK currently.

Package: android-libnativeloader
Section: libs
Architecture: amd64 i386 armhf arm64 riscv64
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
         android-libnativebridge (= ${binary:Version}),
Description: Android native loader library
 This library is only used by Android SDK currently.

Package: dexlist
Architecture: amd64 i386 armhf arm64 riscv64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends},
         android-libbacktrace (>= 1:34),
         android-libnativeloader (= 1:${binary:Version}),
         android-libziparchive,
Description: Lists all methods in all concrete classes in Android DEX files
 This is a re-implementation of the original `dexlist` utility that was
 based on Dalvik functions in `libdex` into a new `dexlist` that is now
 based on ART functions in `libart` instead. The output is very similar
 to the original for correct DEX files. Error messages may differ,
 however. Also, ODEX files are no longer supported.
