NAME = libdexfile_external

SOURCES = libdexfile/external/dex_file_ext.cc

CPPFLAGS += \
  -I/usr/include/android/nativehelper \
  -Ilibartbase \
  -Ilibdexfile \
  -Ilibdexfile/external/include \

LDFLAGS += \
  -shared \
  -Wl,-soname,$(NAME).so.0

debian/out/$(NAME).so.0: $(SOURCES)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	cd debian/out && ln -s $(NAME).so.0 $(NAME).so
